<?php

$goods = [
    [
        'title' => 'Конфета',
        'price_val' => 20,
    ],
    [
        'title' => 'Шоколад',
        'price_val' => 25,
    ],
    [
        'title' => 'Шампунь',
        'price_val' => 50,
    ],
    [
        'title' => 'Зубная паста',
        'price_val' => 60,
    ],
    [
        'title' => 'Зубная счетка',
        'price_val' => 40,
    ],
    [
        'title' => 'Апельсин',
        'price_val' => 10,
    ],
    [
        'title' => 'Гранат',
        'price_val' => 50,
    ],
    [
        'title' => 'Стул',
        'price_val' => 500,
    ],
    [
        'title' => 'Чашка',
        'price_val' => 60,
    ],
    [
        'title' => 'Набор чашек',
        'price_val' => 250,
    ],
];

$userCart = NULL;
if(!empty($_COOKIE['userCart'])) {
    $userCart = json_decode($_COOKIE['userCart'], true);
};

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Shop</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
</head>
<body>
    <div class="container col-md-7">
    <h1>Top 10 best foreign wines</h1>
    <hr>
        <h2>Buy the best wine for the holidays</h2>
<div class="container">
    <div class="row col">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Product Title</th>
                    <th>Value</th>
                    <th>Buy</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($goods as $key => $good):?>
                    <tr>
                        <th scope="row"><?= ++$key; ?></th> 
                        <td><?= $good['title']; ?></td>
                        <td><?= $good['price_val']; ?></td>
                        <td><a class="btn btn-success" href="cart_add.php?buy_item=<?=$key;?>">push</a></td>
                    </tr>
                <?php endforeach; ?>   
            </tbody>
        </table>
    </div> 
</div>
<div class="container col-4 ">
    <?php if(empty($userCart)):?>
        <h2>Empty Basket</h2>
        <?php else:?>
        <h2>Basket</h2>
        <table class="table table-warning">
                <tr class="table-active">
                    <th>Title</th>
                    <th>Amount</th>
                </tr>
                <?php foreach($userCart as $item => $amount):?>   
                    <tr>
                        <td><?=$goods[--$item]['title'];?></td>
                        <td><?=$amount;?></td>
                    </tr>
                <?php endforeach; ?>   
        </table>
    <?php endif; ?>
</div>
<?php require_once $_SERVER['DOCUMENT_ROOT'].'/delete.php' ?>  
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>      
</body>
</html>